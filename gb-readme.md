# Belajar Go dengan tes

_Atau belajar test-driven development dengan Go_

test-driven development (TDD) = pengembangan berbasis tes

* Menjelajahi bahasa Go dengan menulis tes
* **Dapatkan landasan dengan TDD**. Go merupakan bahasa yang baik untuk belajar TDD karena merupakan bahasa yang sederhana untuk dipelajari dan pengetesan terintegrasi di dalamnya
* Percayalah kamu akan mampu memulai menulis sistem yang kokoh dan teruji menggunakan Go

## Latar belakang

I have some experience introducing Go to development teams and have tried different approaches as to how to grow a team from some people curious about Go into highly effective writers of Go systems.

### Apa yang tidak bekerja

#### Baca _sang_ buku

An approach we tried was to take [the blue book](https://www.amazon.co.uk/Programming-Language-Addison-Wesley-Professional-Computing/dp/0134190440) and every week discuss the next chapter along with the exercises.

I love this book but it requires a high level of commitment. The book is very detailed in explaining concepts, which is obviously great but it means that the progress is slow and steady - this is not for everyone.

I found that whilst a small number of people would read chapter X and do the exercises, many people didn't.

#### Memecahkan beberapa masalah

Katas are fun but they are usually limited in their scope for learning a language; you're unlikely to use goroutines to solve a kata.

Another problem is when you have varying levels of enthusiasm. Some people just learn way more of the language than others and when demonstrating what they have done end up confusing people with features the others are not familiar with.

This ends up making the learning feel quite _unstructured_ and _ad hoc_.

### Apa yang bekerja

By far the most effective way was by slowly introducing the fundamentals of the language by reading through [go by example](https://gobyexample.com/), exploring them with examples and discussing them as a group. This was a more interactive approach than "read chapter x for homework".

Over time the team gained a solid foundation of the _grammar_ of the language so we could then start to build systems.

This to me seems analogous to practicing scales when trying to learn guitar.

It doesn't matter how artistic you think you are, you are unlikely to write good music without understanding the fundamentals and practicing the mechanics.

### Apa yang bekerja untukku

When _I_ learn a new programming language I usually start by messing around in a REPL but eventually, I need more structure.

What I like to do is explore concepts and then solidify the ideas with tests. Tests verify the code I write is correct and documents the feature I have learned.

Taking my experience of learning with a group and my own personal way I am going to try and create something that hopefully proves useful to other teams. Learning the fundamentals by writing small tests so that you can then take your existing software design skills and ship some great systems.

## Untuk siapa ini

* Orang-orang yang tertarik mempelajari Go
* Orang-orang yang mengetahui beberapa hal mengenai Go, tetapi ingin menjelajahi pengetesan lebih jauh

## Apa yang kamu perlukan

* Sebuah komputer!
* [Go sudah terpasang di komputer](https://golang.org/)
* Sebuah editor teks
* Mempunyai beberapa pengalaman dengan pemrograman. Memahami konsep seperti `if`, variabel, fungsi, dan lain sebagainya.
* Nyaman menggunakan terminal

## Umpan balik

* Add issues or [tweet me @quii](https://twitter.com/quii)

[MIT license](LICENSE.md)
