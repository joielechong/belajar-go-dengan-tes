package integers

import (
	"fmt"
	"testing"
)

func TestPenambah(t *testing.T) {
	jumlah := Tambah(2, 2)
	diharapkan := 4

	if jumlah != diharapkan {
		t.Errorf("diharapkan '%d' tapi dapat '%d'", diharapkan, jumlah)
	}
}

func ExampleTambah() {
	jumlah := Tambah(1, 5)
	fmt.Println(jumlah)
	// Output: 6
}
