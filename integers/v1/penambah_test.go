package integers

import "testing"

func TestPenambah(t *testing.T) {
    jumlah := Tambah(2, 2)
    diharapkan := 4

    if jumlah != diharapkan {
        t.Errorf("diharapkan '%d' tetapi dapat '%d'", diharapkan, jumlah)
    }
}
