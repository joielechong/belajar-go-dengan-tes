# Halo, Dunia

[**Kamu bisa mendapatkan seluruh kode untuk bab ini di sini**](https://gitlab.com/joielechong/belajar-go-dengan-tes/blob/master/halo-dunia)

Sudah menjadi tradisi untuk membuat program Halo, Dunia saat pertama sekali memulai belajar bahasa pemrograman baru. Buat sebuah file dengan nama `halo.go` dan tulis kode di bawah ini. Untuk menjalankannya ketik `go run halo.go`.

```go
package main

import "fmt"

func main() {
    fmt.Println("Halo, dunia")
}
```

## Bagaimana cara kerjanya

Ketika kamu menulis sebuah program dalam Go, kamu akan memiliki sebuah package `main` yang didefinisikan dengan func `main` di dalamnya. Kata kunci `func` merupakan cara kamu untuk mendefinisikan sebuah fungsi yang memiliki nama dan tubuh.

Dengan `import "fmt"` kita memasukkan sebuah package yang mengandung fungsi `Println` yang kita gunakan untuk mencetak tulisan ke layar ouput.

## Bagaimana melakukan tes

Bagaimana kamu melakukan tes kode sebelumnya? Merupakan hal yang baik untuk memisahkan kode "domain" dari dunia luar \(efek-samping\). Hasil dari kode `fmt.Println` merupakan sebuah efek-samping \(mencetak ke stdout\) dan string yang kita kirim merupakan domain kita.

Jadi, mari kita memisahkan hal-hal tersebut sehingga mudah dites

```go
package main

import "fmt"

func Halo() string {
    return "Halo, dunia"
}

func main() {
    fmt.Println(Halo())
}
```

Kita telah membuah sebuah fungsi baru lagi menggunakan `func` tetapi kali ini kita menambahkan kata kunci lainnya, yakni `string` di definisi. Ini berarti fungsi tersebut mengembalikan sebuah `string` sebagai hasil prosesnya.

Sekarang buat file baru bernama `halo_test.go` tempat kita akan menulis tes untuk fungsi `Halo`

```go
package main

import "testing"

func TestHalo(t *testing.T) {
    got := Halo()
    want := "Halo, dunia"

    if got != want {
        t.Errorf("got '%s' ingin '%s'", got, want)
    }
}
```

Sebelum penjelasan, mari kita menjalankan kode tersebut. Jalankan `go test` di terminal. Tes harus lulus! Untuk melakukan pemeriksaan, coba sengaja rusakkan tes dengan mengubah string `ingin`.

Perhatikanlah bahwa kamu tidak perlu memilih framework pengetesan atau menafsirkan sebuah DSL pengetesan untuk menulis tes. Semua yang kamu perlukan sudah terintegrasi di dalam bahasa Go dan sintaksisnya sama dengan kode yang akan kamu buat di kemudian waktu.

### Menulis tes

Menulis sebuah tes sama seperti menulis sebuah fungsi, dengan beberapa aturan

* Tes harus berada di dalam file yang memiliki nama seperti `xxx_test.go`
* Fungsi tes harus diawali dengan kata `Test`
* Fungsi tes hanya memiliki satu argumen yakni `t *testing.T`

Untuk saat ini, sudah cukup untuk diketahui bahwa `t` dari tipe `*testing.T` merupakan "kait" mu ke framework pengetesan sehingga kamu bisa menggunakan beberapa fungsi contohnya `t.Fail()` saat kamu ingin mengagalkan tes.

#### Hal-hal baru

**if**

Pernyataan If di Go sangat mirip dengan bahasa pemrograman lainnya.

**Deklarasi variabel**

Kita mendeklarasikan beberapa variabel menggunakan sintaksis `varNama := nilai`, yang memampukan kita menggunakan kembali beberapa nilai di dalam tes sehingga kode mudah dibaca. Pada kode `got := Halo()`, berarti kita mendapatkan (got) dan menyimpan hasil dari pemanggilan `Halo()` ke variabel bernama `have`

**t.Errorf**

Kita memanggil _method_ `Errorf` dari `t` yang kemudian akan mencetak pesan ke layar dan mengagalkan tesnya. Huruf `f` merupakan singkatan dari format yang memungkinkan kita membangun sebuah string dengan nilai yang dimasukkan ke dalam nilai karakter pengganti (placeholder) `%s`. Ketika kamu membuat tes gagal kamu akan mengetahui bagaimana method tersebut bekerja.

Kita nanti akan menjelajahi perbedaan antara method dan fungsi.

### Go doc

Fitur hidup berkualitas lainnya dari Go adalah dokumentasi. Kamu bisa meluncurkan dokumentasi secara lokal dengan menjalankan `godoc -http :8000`. Jika kamu membuka halaman [localhost:8000/pkg](http://localhost:8000/pkg), kamu akan melihat semua package yang terpasang di perangkat komputermu.

Sebagian besar library standar memiliki dokumentasi yang sangat baik yang disertai dengan contoh. Kunjungi halaman [http://localhost:8000/pkg/testing/](http://localhost:8000/pkg/testing/) untuk melihat hal-hal yang tersedia untukmu.

### Halo, KAMU

Sekarang kita memiliki tes yang bisa diiterasi secara aman pada software kita.

Pada contoh sebelumnya kita menulis tes _setelah_ kode telah ditulis. Hal tersebut ditujukan agar kamu bisa mendapat contoh bagaimana membuat tes dan mendeklarasikan fungsi. Mulai dari sekarang, kita akan _menulis tes terlebih dahulu_.

Untuk keperluan pengetesan selanjutnya, kita perlu menentukan penerima pesan Halo.

Kita memulainya dengan menangkap keperluan tersebut di dalam sebuat tes. Ini merupakan bagian dasar dari test driven development yang memastikan tes yang kita lakukan memang _sesungguhnya_ mengetes apa yang kita mau. Ketika kamu menulis tes dengan meninjau kebelakang, ada kemungkinan risiko tes yang kamu buat tetap lulus meskipun kode yang kamu buat tidak bekerja seperti yang seharusnya.

```go
package main

import "testing"

func TestHalo(t *testing.T) {
    got := Halo("Ucok")
    want := "Halo, Ucok"

    if got != want {
        t.Errorf("got '%s' want '%s'", got, want)
    }
}
```

Sekarang jalankan `go test`, kamu akan mendapatkan kesalahan kompilasi

```text
./halo_test.go:6:14: too many arguments in call to Halo
    have (string)
    want ()
```

Go merupakan bahasa bertipe statis (statically typed language). Ini berarti kamu tidak dapat mengubah tipe variabel yang telah kamu tetapkan sebelumnya. Hal ini dikarenakan tipe dikaitkan dengan variabel bukan dengan nilai yang diacunya.

Ketika menggunakan bahasa bertipe statis seperti Go, penting untuk _mendengarkan kompiler_. Kompiler memahami bagaimana kodemu seharusnya saling menyatu dan bekerja sama.

Pada kesalahan sebelumnya, kompiler memberitahukan kamu apa yang harus kamu lakukan selanjutnya. Yakni, kita harus mengubah fungsi `Halo` sebelumnya agar dapat menerima argumen.

Ubah fungsi `Halo` agar dapat menerima argumen bertipe string

```go
func Halo(nama string) string {
    return "Halo, Dunia"
}
```

Jika kamu mencoba menjalankan tes, kode di `main.go` akan gagal dikompilasi karena kamu belum memberikan argumen. Berikan string "dunia" sebagai argumen

```go
func main() {
    fmt.Println(Halo("Dunia"))
}
```

Sekarang saat kamu menjalankan test, kamu akan melihat sesuatu seperti ini

```text
halo_test.go:10: have "Halo, Dunia" want "Halo, Ucok"
```

Akhirnya kita mendapatkan program yang bisa dikompilasi tetapi ia belum memenuhi syarat yang ditentukan oleh tes.

Mari kita membuat tes lulus dengan menggunakan argumen nama dan menggabungkannya dengan string `Halo`

```go
func Halo(nama string) string {
    return "Halo, " + nama
}
```

Tes sekarang akan lulus jika kamu jalankan. Biasanya, sebagai bagian dari siklus TDD, kita sekarang seharusnya melakukan _refactor_.

Masih sedikit sekali yang bisa di-refactor saat ini, namun kita bisa memperkenalkan fitur bahasa Go lainnya yakni _konstanta_.

### Konstanta

Konstanta didefinisikan sebagai berikut

```go
const awalanHalo = "Halo, "
```

Kita bisa me-refactor kode sebelumnya menjadi

```go
const awalanHalo = "Halo, "

func Halo(nama string) string {
    return awalanHalo + nama
}
```

Setelah melakukan refactor, jalankan kembali tes untuk memastikan perubahan yang kamu lakukan benar.

Konstanta akan meningkatkan kinerja aplikasimu. Ini karena dengan konstanta instansi string `"Halo, "` tidak akan dilakukan berulang kali saat dipanggil seperti halnya yang terjadi saat tidak menggunakan konstanta.

Sebenarnya peningkatan kinerja aplikasi yang terjadi  pada contoh hampir tidak kelihatan! Tetapi yang ingin ditekankan di sini adalah, kita menggunakan konstanta untuk menangkap arti dari nilai dan mencoba meningkatkan kinerja aplikasi.

## Halo, dunia... lagi

Selanjutnya kita perlu melakukan sesuatu agar ketika fungsi dipanggil dengan string kosong, fungsi tersebut akan menampilkan teks berupa "Halo, Dunia", bukan "Halo, ".

Kita memulai dengan membuat tes yang gagal

```go
func TestHalo(t *testing.T) {

    t.Run("mengatakan halo ke orang", func(t *testing.T) {
        got := Halo("Ucok")
        want := "Halo, Ucok"

        if got != want {
            t.Errorf("got '%s' want '%s'", got, want)
        }
    })

    t.Run("katakan halo dunia ketika diberikan teks kosong", func(t *testing.T) {
        got := Halo("")
        want := "Halo, Dunia"

        if got != want {
            t.Errorf("got '%s' want '%s'", got, want)
        }
    })

}
```

Di sini kita memperkenalkan perkakas tes lainnya dari gudang perkakas pengetesan, yakni subtes. Terkadang sangat berguna untuk mengelompokkan tes-tes dalam satu lingkup yang sama dan kemudian membuat subtes yang menggambarkan skenario yang berbeda.

Keuntungan dari pendekatan ini adalah kamu bisa menentukan kode yang bisa dibagi dan dipergunakan ulang di tes lain.

Ada pengulangan kode saat kita melakukan pemeriksaan apakah pesan yang didapat sesuai dengan yang diharapkan.

Refactor tidak _hanya_ untuk kode produksi!

Penting diingat bahwa tes yang kamu buat merupakan _spesifikasi yang jelas_ tentang apa yang perlu dilakukan oleh kode.

Kita dapat dan perlu melakukan refactor tes.

```go
func TestHalo(t *testing.T) {

	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got '%s' want '%s'", have, want)
		}
	}

	t.Run("katakan halo pada seseorang", func(t *testing.T) {
		got := Halo("Ucok")
		want := "Halo, Ucok"
		assertCorrectMessage(t, got, want)
	})

	t.Run("teks kosong default menjadi 'Dunia'", func(t *testing.T) {
		got := Halo("")
		want := "Halo, Dunia"
		assertCorrectMessage(t, got, want)
	})

}
```

Apa yang telah kita lakukan di sini?

Kita telah merefaktor assertion menjadi fungsi. Ini mengurangi duplikasi dan meningkatkan kemudahan baca dari tes kita. Dalam Go, kamu dapat mendeklarasikan fungsi di dalam fungsi lain dan menetapkannya ke variable. Kamu dapat memanggilnya seperti fungsi normal. Kita perlu lulus di `t *testing.T` sehingga kita dapat memerintahkan kode tes agar gagal saat kita memerlukannya.

`t.Helper()` diperlukan untuk memberitahuan perkakas tes bahwa method ini merupakan helper. Dengan melakukan hal tersebut, saat tes gagal nomor baris yang dilaporkan akan berada di dalam _pemanggilan fungsi_ kita bukan di dalam helper tes. Hal ini akan membantu dan memudahkan pengembang lainnya dalam mencari sumber masalah. Jika mau belum memahaminya, coba jadikan komentar kode `t.Helper()`, buat sebuah tes gagal, lalu amati keluaran tes.

Sekarang kita memiliki tes gagal yang telah ditulis dengan baik, mari perbaiki kodenya dengan menggunakan `if`.

```go
const awalanHalo = "Halo, "

func Halo(nama string) string {
    if nama == "" {
        nama = "Dunia"
    }
    return awalanHalo + nama
}
```

Jika kita menjalankan tes, kita akan mendapatkan bahwa perubahan kode di atas memenuhi persyaratan tes dan kita tidak secara sengaja merusak fungsionalitas lainnya.

### Disiplin

Mari kita telaah siklusnya lagi

* Tulis sebuah tes
* Buat kompiler lulus
* Jalankan tes, lihat apakah gagal dan periksa pesan kesalahannya masuk akal
* Tulis kode secukupnya untuk membuat tes lulus
* Refaktor

Jika diamati langkah di atas kelihatan melelahkan, tetapi tetap melekat pada lup umpan balik adalah penting.

Tidak hanya hal tersebut memastikan kamu memiliki _tes yang berkaitan_ ia juga menolong memastikan _kamu merancang perangkat lunak yang baik_ dengan merefaktor berdasarkan tes sebagai pengaman.

Melihat test gagal merupakan pemeriksaan penting karena hal itu memberikan kamu bagaimana pesan kesalahannya terlihat. Sebagai pengembang, dapat menjadi pekerjaan berat untuk bekerja dengan codebase ketika tes yang gagal tidak memberikan gagasan jernih tentang masalahnya.


Dengan memastikan tes kamu _cepat_ dan menyetel perkakasmu agar dapat menjalankan secara mudah, maka kamu akan bisa masuk ke kondisi _flow_ saat menulis kodemu.

Dengan tidak menulis tes, kamu membaktikan dirmu ke pemeriksaan kode secara manual dengan menjalankan perangkat lunak yang dapat  merusak kondisi flow-mu dan kamu tidak akan mampu menghemat waktumu, terutama untuk jangka panjang.

## Terus maju! Lebih banyak permintaan

Ya ampun, kita memiliki lebih banyak permintaan. Kita sekarang perlu untuk mendukung parameter kedua, menentukan bahasa dari sapaan. Jika bahasa diberikan tetapi tidak kita kenali, maka otomatis gunakan bahasa Indonesia.

Kita harus percaya diri bahwa kita dapat menggunakan TDD untuk mengembangkan fungsionalitas tersebut secara mudah!

Tulis sebuah tes untuk pengguna yang memberikan bahasa Spanyol. Tambahkan ke rangkaian pengujian yang telah ada.

```go
    t.Run("dalam Spanyol", func(t *testing.T) {
        got := Halo("Elodie", "Spanyol")
        want := "Hola, Elodie"
        assertCorrectMessage(t, got, want)
    })
```

Ingat untuk tidak curang! _Tes terlebih dahulu_. Ketika kamu mencoba dan menjalankan tes, kompiler _seharusnya_ mengomel karena kamu memanggil `Halo` menggunakan dua argumen ketimbang satu.

```text
./halo_test.go:27:14: too many arguments in call to Halo
    have (string, string)
    want (string)
```

Perbaiki masalah kompilasi dengan menambahkan argumen string lain ke `Halo`

```go
func Halo(name string, bahasa string) string {
    if nama == "" {
        nama = "Dunia"
    }
    return awalanHalo + nama
}
```

Ketika kamu mencoba dan menjalankan tes lagi, ia akan mengomel mengenai tidak memberikan argumen yang cukup ke `Helo` di tes yang lain dan di `halo.go`

```text
./halo.go:15:18: not enough arguments in call to Halo
    have (string)
    want (string, string)
```

Perbaiki mereka dengan memberikan string kosong. Sekarang semua tesmu akan dikompilasi dan lulus, terpisah dari skenario kita yang baru

```text
halo_test.go:29: got 'Halo, Elodie' want 'Hola, Elodie'
```

Kata dapat menggunakan `if` di sini untuk memeriksa apakah bahasa sama dengan "Spanyol", jika sama maka ubah pesan

```go
func Halo(nama string, bahasa string) string {
    if nama == "" {
        nama = "Dunia"
    }

    if bahasa == "Spanyol" {
        return "Hola, " + nama
    }

    return awalanHalo + nama
}
```

Tes seharusnya sudah lulus sekarang.

Saatnya untuk melakukan _refaktor_. Kamu akan melihat masalah yang sama di kode, string "ajaib", beberapa diulangi. Coba dan refaktor hal tersebut sendiri, dengan semua perubahan pastikan kamu menjalankan ulang tes untuk meyakinkan refaktor yang kamu kerjakan tidak merusak apapun.

```go
const spanyol = "Spanyol"
const awalanHalo = "Halo, "
const awalanHaloSpanyol = "Hola, "

func Halo(nama string, bahasa string) string {
    if nama == "" {
        nama = "Dunia"
    }

    if bahasa == spanyol {
        return awalanHaloSpanyol + nama
    }

    return awalanHalo + nama
}
```

### Prancis

* Tulis sebuah tes yang memastikan jika kamu memberikan `"Prancis"` kamu mendapatkan `"Bonjour, "`
* Perhatikan jika gagal, periksa pesan kesalahan gampang dibaca
* Lakukan perubahan masuk akal yang paling sedikit di kode

Kamu mungkin menulis sesuatu yang kira-kira seperti ini

```go
func Halo(nama string, bahasa string) string {
    if nama == "" {
        nama = "Dunia"
    }

    if bahasa == spanyol {
        return awalanHaloSpanyol + nama
    }

    if bahasa == prancis {
        return awalanHaloPrancis + nama
    }

    return awalanHalo + nama
}
```

## `switch`

Ketika kamu memiliki banyak pernyataan `if` untuk memeriksa nilai tertentu, umumnya digunakan pernyataan `switch` sebagai penggantinya. Kita dapat menggunakan `switch` untuk merefaktor kode sehingga lebih mudah dibaca dan lebih gampang dikembangkan lebih lanjut ssat kita ingin menambahkan dukungan terhadap bahasa lainnya di kemudian waktu.

```go
func Halo(nama string, bahasa string) string {
    if nama == "" {
        nama = "Dunia"
    }

    awalan := awalanHalo

    switch bahasa {
    case french:
        awalan = awalanHaloPrancis
    case spanish:
        awalan = awalanHaloSpanyol
    }

    return awalan + nama
}
```

Tulis sebuah tes untuk mencakup sapaan dalam bahasa pilihanmu maka kamu akan melihat betapa mudahnya untuk mengembangkan fungsi kita _yang luar biasa_.

### satu...terakhir...refaktor?

Kamu dapat memperlihatkan bahwa mungkin fungsi kita semakin membesar. Refaktor paling sederhana untuknya adalah dengan memisahkan beberapa fungsionalitas menjadi sebuah fungsi berbeda.

```go
func Halo(nama string, bahasa string) string {
    if nama == "" {
        nama = "Dunia"
    }

    return awalanSapaan(bahasa) + nama
}

func awalanSapaan(bahasa string) (awalan string) {
    switch bahasa {
    case prancis:
        awalan = awalanHaloPrancis
    case spanyol:
        awalan = awalanHaloSpanyol
    default:
        awalan = awalanIndonesia
    }
    return
}
```

Beberapa konsep baru:

* Pada function signature kita, kita telah membuat _nilai kembalian bernama_ `(awalan string)`.
*Ini akan membuat variable bernama `awalan` di fungsimu.
  * Ia akan diberikan nilai awal "kosong". Ini bergantung pada jenis variabelnya, sebagai contoh, `int` adalah 0 dan untuk string adalah `""`.
    * Kamu bisa mengembalikan nilai apapun yang ditetapkan dengan hanya memanggil `return` daripada `return awalan`.
  * Ini akan ditampilkan di Go Doc fungsimu jadi ia akan membuat tujuan kodemu lebih jelas.
* `default` pada perintah switch akan dikerjakan jika tidak ada pernyataan `case` lainnya yang cocok.
* Nama fungsi dimulai dengan hurut kecil. Dalam bahasa Go, fungsi publik dimulai dengan huruf besar dan fungsi private dimulai dengan huruf kecil. Kita tidak mau algoritma internal kita ditampakkan ke dunia jadi kita membuat fungsi ini privat.

## Penutupan

Siapa sangka kamu bisa mendapatkan banyak hal dari `Halo, Dunia`?

Sekarang kamu sudah memiliki pemahaman tentang:

### Beberapa sintaks GO mengenai

* Menulis tes
* Menyatakan fungsi, dengan argumen dan jenis pengembalian
* `if`, `const` dan `switch`
* Menyatakan variabel dan konstanta

### Proses TDD dan _mengapa_ langkah-langkanya penting

* _Menulis tes gagal dan melihatnya gagal_ sehingga kita mengetahui bahwa kita telah menulis sebuah tes yang _berkaitan_ untuk permintaan kita dan melihat ia memberikan _deskripsi kesalahan yang mudah dimengerti_
* menulis jumlah kode sedikit mungkin yang diperlukan untuk membuatnya lulus sehingga kita mengetahui bahwa kita memiliki perangkat lunak yang bekerja
* Refaktor _kemudian_, yang didukung dengan pengamanan dari tes kita untuk memastikan kita memiliki kode yang telah dibuat dengan baik dan mudah untuk dipakai

Pada kasus kita, kita telah memulai dari `Halo()` ke `Halo("nama")`, hingga `Halo("nama", "prancis")` dalam langkah kecil yang mudah dimengerti.

Hal ini tentunya sederhana dibandingkan dengan perangkat "dunia nyata" tetapi prinsipnya tetap berlaku. TDD merupakan keahlian yang memerlukan latihan untuk mengembangkannya, tetapi dengan kemampuan untuk memecahkan masalah menjadi bagian-bagian yang lebih kecil yang dapat diuji, kamu akan memiliki waktu yang jauh lebih mudah dalam menulis perangkat lunak.

