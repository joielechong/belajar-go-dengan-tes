# Belajar Go dengan tes

_Atau belajar test-driven development dengan Go_

test-driven development \(TDD\) = pengembangan berbasis tes

![Build Status](https://travis-ci.org/quii/learn-go-with-tests.svg?branch=master)  
[![Go Report Card](https://goreportcard.com/badge/github.com/quii/learn-go-with-tests)](https://goreportcard.com/report/github.com/quii/learn-go-with-tests)

[Kamu bisa baca buku ini di Gitbook](https://quii.gitbook.io/learn-go-with-tests)

Translations: [中文](https://studygolang.gitbook.io/learn-go-with-tests)

## Mengapa

* Menjelajahi bahasa Go dengan menulis tes
* **Mendapatkan landasan dasar mengenai TDD**. Go merupakan bahasa yang bagus untuk belajar TDD karena mudah dipelajari dan pengetesan sudah terintegrasi di dalamnya
* Percaya diri bahwa kamu akan mampu memulai membuat sistem yang kokoh dan teruji menggunakan Go

## Daftar isi

Jika tidak ada tautan, berarti belum selesai! [mengapa tidak mencoba ambil peran?](contributing.md)

### Dasar-dasar Go

1. [Memasang Go](memasang-go.md) - Menyiapkan lingkungan untuk produktivitas.
2. [Halo, dunia](halo-dunia.md) - Mendeklarasikan variabel, konstanta, pernyataan if/else, switch, menulis program Go pertamamu dan menulis tes pertamamu. Sintaks sub-tes dan closure.
3. [Integer](integers.md) - Penjelajahan lebih jauh mengenai sintaks penetapan fungsi dan mempelajari cara baru untuk meningkatkan dokumentasi di kodemu.
4. [Iteration](iteration.md) - Learn about `for` and benchmarking.
5. [Arrays and slices](arrays-and-slices.md) - Learn about arrays, slices, `len`, varargs, `range` and test coverage.
6. [Structs, methods & interfaces](structs-methods-and-interfaces.md) - Learn about `struct`, methods, `interface` and table driven tests.
7. [Pointers & errors](pointers-and-errors.md) - Learn about pointers and errors.
8. [Maps](maps.md) - Learn about storing values in the map data structure.
9. [Dependency Injection](dependency-injection.md) - Learn about dependency injection, how it relates to using interfaces and a primer on io.
10. [Mocking](mocking.md) - Take some existing untested code and use DI with mocking to test it.
11. [Concurrency](concurrency.md) - Learn how to write concurrent code to make your software faster.
12. [Select](select.md) - Learn how to synchronise asynchronous processes elegantly.
13. [Reflection](reflection.md) - Learn about reflection

Property-based tests \(todo\)

### Membangun sebuah aplikasi

Now that you have hopefully digested the _Go Fundamentals_ section you have a solid grounding of a majority of Go's language features and how to do TDD.

This next section will involve building an application.

Each chapter will iterate on the previous one, expanding the application's functionality as our product owner dictates.

New concepts will be introduced to help facilitate writing great code but most of the new material will be learning what can be accomplished from Go's standard library.

By the end of this, you should have a strong grasp as to how to iteratively write an application in Go, backed by tests.

* [HTTP server](http-server.md) - We will create an application which listens to HTTP requests and responds to them.
* [JSON, routing and embedding](json.md) - We will make our endpoints return JSON and explore how to do routing.
* [IO and sorting](io.md) - We will persist and read our data from disk and we'll cover sorting data.
* [Command line \(TODO\)](command-line.md) - Working with flags and responding to user input on the command line

## Andil

* _This project is work in progress_ If you would like to contribute, please do get in touch.
* Read [contributing.md](https://github.com/quii/learn-go-with-tests/tree/842f4f24d1f1c20ba3bb23cbc376c7ca6f7ca79a/contributing.md) for guidelines
* Ada ide? Buat issue

## Latar belakang

I have some experience introducing Go to development teams and have tried different approaches as to how to grow a team from some people curious about Go into highly effective writers of Go systems.

### Apa yang tidak bekerja

#### Baca _sang_ buku

An approach we tried was to take [the blue book](https://www.amazon.co.uk/Programming-Language-Addison-Wesley-Professional-Computing/dp/0134190440) and every week discuss the next chapter along with the exercises.

I love this book but it requires a high level of commitment. The book is very detailed in explaining concepts, which is obviously great but it means that the progress is slow and steady - this is not for everyone.

I found that whilst a small number of people would read chapter X and do the exercises, many people didn't.

#### Memecahkan beberapa masalah

Katas are fun but they are usually limited in their scope for learning a language; you're unlikely to use goroutines to solve a kata.

Another problem is when you have varying levels of enthusiasm. Some people just learn way more of the language than others and when demonstrating what they have done end up confusing people with features the others are not familiar with.

This ends up making the learning feel quite _unstructured_ and _ad hoc_.

### Apa yang bekerja

By far the most effective way was by slowly introducing the fundamentals of the language by reading through [go by example](https://gobyexample.com/), exploring them with examples and discussing them as a group. This was a more interactive approach than "read chapter x for homework".

Over time the team gained a solid foundation of the _grammar_ of the language so we could then start to build systems.

This to me seems analogous to practicing scales when trying to learn guitar.

It doesn't matter how artistic you think you are, you are unlikely to write good music without understanding the fundamentals and practicing the mechanics.

### Apa yang bekerja untukku

When _I_ learn a new programming language I usually start by messing around in a REPL but eventually, I need more structure.

What I like to do is explore concepts and then solidify the ideas with tests. Tests verify the code I write is correct and documents the feature I have learned.

Taking my experience of learning with a group and my own personal way I am going to try and create something that hopefully proves useful to other teams. Learning the fundamentals by writing small tests so that you can then take your existing software design skills and ship some great systems.

## Untuk siapa ini

* Orang-orang yang tertarik mempelajari Go
* Orang-orang yang mengetahui beberapa hal mengenai Go, tetapi ingin menjelajahi pengetesan lebih jauh

## Apa yang kamu perlukan

* Sebuah komputer!
* [Go sudah terpasang di komputer](https://golang.org/)
* Sebuah editor teks
* Mempunyai beberapa pengalaman dengan pemrograman. Memahami konsep seperti `if`, variabel, fungsi, dan lain sebagainya.
* Nyaman menggunakan terminal

## Umpan balik

* Tambahkan issue atau [tweet saya @quii](https://twitter.com/quii)

[MIT license](LICENSE.md)

[Logo is by egonelbre](https://github.com/egonelbre) What a star!

