# Daftar isi

* [Belajar Go dengan tes](gb-readme.md)

## Dasar-dasar Go

* [Memasang Go](memasang-go.md)
* [Halo, Dunia](halo-dunia.md)
* [Integers](integers.md)
* [Iteration](iteration.md)
* [Arrays and slices](arrays-and-slices.md)
* [Structs, methods & interfaces](structs-methods-and-interfaces.md)
* [Pointers & errors](pointers-and-errors.md)
* [Maps](maps.md)
* [Dependency Injection](dependency-injection.md)
* [Mocking](mocking.md)
* [Concurrency](concurrency.md)
* [Select](select.md)
* [Reflection](reflection.md)

## Membangun sebuah aplikasi

* [Intro](app-intro.md)
* [HTTP server](http-server.md)
* [JSON, routing and embedding](json.md)
* [IO and sorting](io.md)
* [Command line (todo)](command-line.md)
* `time` (todo)

## Meta

* [Contributing](contributing.md)
* [Template Bab](template.md)
