package main

import "fmt"

const awalanHalo = "Halo, "

// Halo mengembalikan sebuah sapaan personal
func Halo(nama string) string {
	return awalanHalo + nama
}

func main() {
	fmt.Println(Halo("Dunia"))
}
