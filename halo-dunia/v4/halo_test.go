package main

import "testing"

func TestHalo(t *testing.T) {
	got := Halo("Ucok")
	want := "Halo, Ucok"

	if got != want {
		t.Errorf("got '%s' want '%s'", got, want)
	}
}
