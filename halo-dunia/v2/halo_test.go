package main

import "testing"

func TestHalo(t *testing.T) {
	got := Halo()
	want := "Halo, Dunia"

	if got != want {
		t.Errorf("got '%s' want '%s'", got, want)
	}
}
