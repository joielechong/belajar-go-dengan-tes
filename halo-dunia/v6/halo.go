package main

import "fmt"

const spanyol = "Spanyol"
const prancis = "Prancis"
const awalanHalo = "Halo, "
const awalanHaloSpanyol = "Hola, "
const awalanHaloPrancis = "Bonjour, "

// Halo mengembalikan sapaan personal berdasarkan bahasa
func Halo(nama string, bahasa string) string {
	if nama == "" {
		nama = "Dunia"
	}

	if bahasa == spanyol {
		return awalanHaloSpanyol + nama
	}

	if bahasa == prancis {
		return awalanHaloPrancis + nama
	}

	return awalanHalo + nama
}

func main() {
	fmt.Println(Halo("Dunia", ""))
}
