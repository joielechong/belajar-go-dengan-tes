package main

import "fmt"

// Halo mengembalikan sebuah sapaan personal
func Halo(nama string) string {
	return "Halo, " + nama
}

func main() {
	fmt.Println(Halo("Dunia"))
}
