package main

import "testing"

func TestHello(t *testing.T) {

	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	}

	t.Run("mengatakan halo ke orang", func(t *testing.T) {
		got := Halo("Ucok")
		want := "Halo, Ucok"
		assertCorrectMessage(t, got, want)
	})

	t.Run("teks kosong default menjadi 'Dunia'", func(t *testing.T) {
		got := Halo("")
		want := "Halo, Dunia"
		assertCorrectMessage(t, got, want)
	})

}
