package main

import "fmt"

const awalanHalo = "Halo, "

// Halo mengembalikan sapaan personal, mengembalikan bawaan Halo, Dunia jika nama kosong diberikan
func Halo(nama string) string {
	if nama == "" {
		nama = "Dunia"
	}
	return awalanHalo + nama
}

func main() {
	fmt.Println(Halo("Dunia"))
}
