package main

import "fmt"

const spanyol = "Spanyol"
const prancis = "Prancis"
const awalanHalo = "Halo, "
const awalanHaloSpanyol = "Hola, "
const awalanHaloPrancis = "Bonjour, "

// Halo mengembalikan sapaan personal berdasarkan bahasa
func Halo(nama string, bahasa string) string {
	if nama == "" {
		nama = "Dunia"
	}

	awalan := awalanHalo

	switch bahasa {
	case prancis:
		awalan = awalanHaloPrancis
	case spanyol:
		awalan = awalanHaloSpanyol
	}

	return awalan + nama
}

func main() {
	fmt.Println(Halo("Dunia", ""))
}
