package main

import "testing"

func TestHello(t *testing.T) {

	assertCorrectMessage := func(got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	}

	t.Run("katakan halo pada seseorang", func(t *testing.T) {
		got := Halo("Chris", "")
		want := "Halo, Chris"
		assertCorrectMessage(got, want)
	})

	t.Run("katakan halo saat teks kosong diberikan", func(t *testing.T) {
		got := Halo("", "")
		want := "Halo, Dunia"
		assertCorrectMessage(got, want)
	})

	t.Run("katakan halo dalam Spanyol", func(t *testing.T) {
		got := Halo("Elodie", spanyol)
		want := "Hola, Elodie"
		assertCorrectMessage(got, want)
	})

	t.Run("katakan halo dalam Prancis", func(t *testing.T) {
		got := Halo("Lauren", prancis)
		want := "Bonjour, Lauren"
		assertCorrectMessage(got, want)
	})

}
