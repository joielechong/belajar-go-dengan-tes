package main

import "fmt"

const spanyol = "Spanyol"
const prancis = "Prancis"
const awalanIndonesia = "Halo, "
const awalanHaloSpanyol = "Hola, "
const awalanHaloPrancis = "Bonjour, "

// Halo mengembalikan pesan personal berdasarkan bahasa
func Halo(nama string, bahasa string) string {
	if nama == "" {
		nama = "Dunia"
	}

	return awalanSapaan(bahasa) + nama
}

func awalanSapaan(bahasa string) (awalan string) {
	switch bahasa {
	case prancis:
		awalan = awalanHaloPrancis
	case spanyol:
		awalan = awalanHaloSpanyol
	default:
		awalan = awalanIndonesia
	}
	return
}

func main() {
	fmt.Println(Halo("Dunia", ""))
}
