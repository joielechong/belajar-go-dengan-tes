package main

import "testing"

func TestHalo(t *testing.T) {

	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	}

	t.Run("ke seseorang", func(t *testing.T) {
		got := Halo("Ucok", "")
		want := "Halo, Ucok"
		assertCorrectMessage(t, got, want)
	})

	t.Run("teks kosong", func(t *testing.T) {
		got := Halo("", "")
		want := "Halo, Dunia"
		assertCorrectMessage(t, got, want)
	})

	t.Run("dalam Spanyol", func(t *testing.T) {
		got := Halo("Elodie", spanyol)
		want := "Hola, Elodie"
		assertCorrectMessage(t, got, want)
	})

	t.Run("dalam Prancis", func(t *testing.T) {
		got := Halo("Lauren", prancis)
		want := "Bonjour, Lauren"
		assertCorrectMessage(t, got, want)
	})

}
