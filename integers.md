# Integers

**[Kamu bisa mendapatkan seluruh kode untuk bab ini di sini](https://gitlab.com/joielechong/belajar-go-dengan-tes/tree/master/integers)**

Integer bekerja seperti yang kamu harapkan. Mari menulis sebuah fungsi penambahan untuk mencobanya. Buat sebuah berkas tes yang bernama `penambah_test.go` dan tulis kode berikut.

**catatan:** Berkas kode Go hanya dapat memiliki satu `package` per direktori, pastikan semua berkasmu disusun secara terpisah. [Ini penjelasan yang bagus tentang hal tersebut.](https://dave.cheney.net/2014/12/01/five-suggestions-for-setting-up-a-go-project)


## Buat tes terlebih dahulu

```go
package integers

import "testing"

func TestPenambah(t *testing.T) {
    jumlah := Tambah(2, 2)
    diharapkan := 4

    if jumlah != diharapkan {
        t.Errorf("diharapkan '%d' tetapi dapat '%d'", diharapkan, jumlah)
    }
}
```

Kamu akan melihat bahwa kita menggunakan `%d` sebagai format string daripada `%s`. Ini karena kita ingin menampilkan nilai integer bukan nilai string. Sesuai dengan namanya, ini ditujukan untuk mengelompokkan fungsi yang bekerja dengan integer seperti fungsi Tambah.

## Coba dan jalankan tes

Jalankan tes `go test`

Selidiki kesalahan kompilasi

`./penambah_test.go:6:9: undefined: Tambah`

## Tulis kode seminimal mungkin agar tes berjalan dan periksa keluaran tes yang gagal

Tulis kode secukupnya untuk memenuhi permintaan kompiler _dan hanya itu_ - ingat bahwa kita ingin memeriksa apakah tes kita gagal dengan alasan yang benar.

```go
package integers

func Tambah(x, y int) int {
    return 0
}
```

Ketika kamu memiliki lebih dari satu argumen dengan jenis yang sama \(dalam kasus kita saat ini adalah integer\) daripada menggunakan `(x int, y int)` kamu bisa menyingkatnya menggunakan `(x, y int)`.

Sekarang jalankan tes dan kita harus senang karena tes kita telah memberitahukan apa yang salah secara benar.

`penambah_test.go:10: diharapkan '4' tetapi dapat '0'`

Jika kamu perhatikan, kita telah mempelajari mengenai _nilai kembalian bernama_ di bagian [akhir](halo-dunia.md#satu...terakhir...refaktor?) tetapi tidak menggunakn yang sama di sini. Ia umumnya harus digunakan ketika arti dari hasil tidak jelas dari konteksnya. Pada kasus kita, jelas bahwa fungsi `Tambah` akan menambahkan parameter. Kamu bisa melihat wiki [ini](https://github.com/golang/go/wiki/CodeReviewComments#named-result-parameters) untuk lebih lengkapnya.

## Tulis kode secukupnya untuk membuatnya lulus

Dalam pola pikir TDD terketat, kita sekarang harus menulis _kode sesedikit mungkin untuk membuat tes lulus_. Programmer akademis mungkin menulis seperti ini

```go
func Tambah(x, y int) int {
    return 4
}
```

Ah hah! gagal lagi, TDD ternyata tipu-tipu yah?

Kita dapat menulis tes lainnya, dengan beberapa angka berbeda untuk memaksa tes gagal, tetapi hal tersebut kelihatan seperti permainan kucing-kucingan.

Setelah kita lebih akrab dengan sintaks Go, saya akan memperkenalkan teknik bernama Pengetesan Berbasis Properti, yang akan berhenti menggangu developer dan membantumu mencari bug.

Untuk saat ini, mari perbaiki dengan benar

```go
func Tambah(x, y int) int {
    return x + y
}
```

Jika kamu menjalankan ulang tes, ia akan lulus.

## Refaktor

Tidak ada kode yang cukup _aktual_ yang dapat kita perbaiki di sini.

Di awal kita telah menjelajahi bagaimana memberi nama argumen kembalian sehingga ia muncul di dokumentasi dan juga di kebanyakan editor teks developer.

Hal ini sangat baik karena memberikan pertolongan akan kemudahan penggunaan kode yang kamu tulis. Adalah hal yang diutamakan agar pengguna dapat mengerti kegunaan kodemu hanya dengan melihat type signature dan dkomentasi.

Kamu dapat menambahkan dokumentasi ke fungsi dengan komentar, dan itu akan muncul di Go Doc sama seperti halnya saat kamu melihat dokumentasi pustaka standar.

```go
// Tambah menerima dua integer dan mengembalikan jumlahnya
func Tambah(x, y int) int {
    return x + y
}
```

### Contoh-contoh

Jika kamu ingin menjelajahi lebih jauh, kamu bisa membuat [examples](https://blog.golang.org/examples). Kamu akan melihat banyak contoh-contoh di dokumentasi pustaka standar.

Biasanya kode contoh tidak sewaktu dengan apa yang kode saat ini lakukan karena contoh hidup di luar dari kode nyata dan tidak diperiksa.

Example Go dieksekusi seperti halnya tes, jadi kamu bisa yakin contoh-contoh menunjukkan yang kode sebenarnya lakukan.

Example dikompilasi \(dan dieksekusi opsional\) sebagai bagian dari rangkaian paket tes.

Seperti tes umumnya, example merupakan fungsi yang ada di berkas paket \_test.go. Tambahkan fungsi ExampleTambah ke berkas `penambah_tes.go`.

```go
func ExampleTambah() {
    jumlah := Tambah(1, 5)
    fmt.Println(jumlah)
    // Output: 6
}
```

Jika kamu mengubah kodemu sehingga example-nya tidak lagi valid, maka build kamu akan gagal.

Dengan menjalankan rangkaian tes package, kita dapat melihat fungsi example dijalankan tanpa adanya campur tangan kita:

```bash
$ go test -v
=== RUN   TestPenambah
--- PASS: TestPenambah (0.00s)
=== RUN   ExampleTambah
--- PASS: ExampleTambah (0.00s)
```

Mohon dicatat bahwa fungsi example tidak akan dijalankan jika kamu menghapus komentar "//Output: 6". Meskipun fungsi dikompilasi, tetapi ia tidak akan dijalankan.

Dengan menambahkan kode ini di example, maka ia akan muncul di dokumentasi `godoc` yang semakin membuat kodemu lebih mudah diakses.

##Penutupan

Apa yang telah kita pelajari:

* Lebih banyak latihan alur kerja TDD
* Integer, penjumlahan
* Menulis dokumentasi yang lebih baik sehingga pengguna kode kita dapat memahami kegunaan kode dengan lebih cepat
* Example sebagai contoh penggunaan kode kita, yang diperiksa sebagai bagian dari tes kita
