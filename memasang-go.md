# Memasang Go, menyiapkan lingkungan untuk produktivitas

Instruksi pemasangan resmi Go tersedia di [sini](https://golang.org/doc/install).

Petunjuk di sini mengasumsikan kamu menggunakan manajer paket aplikasi seperti [Homebrew](https://brew.sh) di Apple Macintosh, [Chocolatey](https://chocolatey.org), [Apt](https://help.ubuntu.com/community/AptGet/Howto) di Ubuntu, [yum](https://access.redhat.com/solutions/9934) di Red Hat, atau [sbopkg](https://sbopkg.org/) di Slackware.

kita akan menunjukkan bagaimana prosedur pemasangan Go di OSX menggunakan Homebrew.

## Pemasangan

Proses pemasangan sangatlah sederhana. Pertama, yang perlu kamu lakukan adalah menjalankan perintah berikut untuk terlebih dahulu memasang homebrew (brew). Brew bergantung pada Xcode jadi kamu harus memastikan Xcode telah terpasang sebelumnya.

```sh
xcode-select --install
```

Kemudian jalankan perintah berikut untuk memasang homebrew:

```sh
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Setelahnya kamu bisa memasang Go dengan perintah berikut:

```sh
brew install go
```

Jika kamu ingin meluncurkan program yang akan kamu buat ke server berbasis Linux, maka kamu perlu mengaktifkan fitur kompilasi silang. Kamu dapat melakukannya dengan menggunakan perintah berikut:

```sh
brew install go --cross-compile-common
```

*Kamu sebaiknya mengikuti instruksi yang direkomendasikan oleh manajer paket aplikasi sistem operasimu, karena ada beberapa hal yang mungkin berbeda di jenis sistem operasi tertentu*.

Kamu dapat memeriksa pemasangan dengan:

```sh
$ go version
```
yang akan menampilkan keluaran seperti berikut:

```sh
go version go1.10 darwin/amd64
```

## Environment Go

Go bersifat _opinionated_.  Ini berarti Go menetapkan dan mengarahkan kita sesuai dengan cara dan proses kerja bahasa Go.

Berdasarkan kesepakatan yang telah dibuat, semua kode Go ditempatkan dalam satu ruang kerja (folder). Ruang kerja ini dapat berada di mana saja dalam komputermu. Jika kamu tidak menentukan ruang kerja, maka Go akan mengasumsikan $HOME/go sebagai ruang kerjanya. Ruang kerja dikenali (dan diubah) oleh _variabel environment_ [GOPATH](https://golang.org/cmd/go/#hdr-GOPATH_environment_variable). variabel environment merupakan cara kita menentukan pengaturan lingkungan sistem untuk Go.

Kamu sebaiknya menetapkan variabel environment sehingga nantinya bisa digunakan di script, shell, dan sebagainya.

Perbaharui .bash_profile mu sehingga mengandung kode berikut:

```sh
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin
```

*Catatan* kamu harus membuka shell baru setelah menetapkannya untuk mendapatkan variabel environment tersebut.

Go mengasumsikan ruang kerjamu mencakup struktur direktori tertentu.

Go menetapkan file-filenya di tiga direktori: Semua source code berada di dalam **src**, objek package berada di dalam **pkg**, dan program terkompilasi di dalam **bin**. Kamu dapat membuat direktori tersebut dengan perintah berikut:

```sh
mkdir -p $GOPATH/src $GOPATH/pkg $GOPATH/bin
```

Pada titik ini, kamu bisa menjalankan _go get_ yang akan memasang src/package/bin di direktori $GOPATH/xxx yang sesuai.

## Go Editor

Editor preference is very individualistic, you may already have a preference that supports Go. If you don't you should consider an Editor such as [Visual Studio Code](https://code.visualstudio.com), which has exceptional Go support.

To install VS Code using brew, because this is a GUI application you need an extension to homebrew called cask to support install VS Code.

```sh
brew tap caskroom/cask
```

At this point you can now use brew to install VS Code.

```sh
brew cask install visual-studio-code
```

You can confirm VS Code installed correctly you can run the following in your shell.

```sh
code .
```

VS Code is shipped with very little software enabled, you can enable new software by installing extensions. To add Go support you must install an extension, there are a variety available for VS Code, an exceptional one is [Luke Hoban's package](https://github.com/Microsoft/vscode-go). This can be installed as follows:

```sh
code --install-extension lukehoban.Go
```

When you open a Go file for the first time in VS Code, it will indicate that the Analysis tools are missing, you should click the button to install these. The list of tools that gets installed (and used) by VS Code are available [here](https://github.com/Microsoft/vscode-go/wiki/Go-tools-that-the-Go-extension-depends-on).

## Go Debugger

A good option for debugging Go (that's integrated with VS Code) is Delve. This can be installed as follows using go get:

```sh
go get -u github.com/derekparker/delve/cmd/dlv
```

## Go Linting

An improvement over the default linter can be configured using [Gometalinter](https://github.com/alecthomas/gometalinter).

This can be installed as follows:

```sh
go get -u github.com/alecthomas/gometalinter
gometalinter --install
```

## Refactoring and your tooling

A big emphasis of this book is around the importance of refactoring.

Your tools can help you do bigger refactoring with confidence.

You should be familiar enough with your editor to perform the following with a simple key combination:

- **Extract/Inline variable**. Being able to take magic values and give them a name lets you simplify your code quickly
- **Extract method/function**. It is vital to be able to take a section of code and extract functions/methods
- **Rename**. You should be able to confidently rename symbols across files.
- **go fmt**. Go has an opinioned formatter called `go fmt`. Your editor should be running this on every file save.
- **Run tests**. It goes without saying that you should be able to do any of the above and then quickly re-run your tests to ensure your refactoring hasn't broken anything

In addition, to help you work with your code you should be able to:

- **View function signature** - You should never be unsure how to call a function in Go. Your IDE should describe a function in terms of its documentation, its parameters and what it returns.
- **View function definition** - If it's still not clear what a function does, you should be able to jump to the source code and try and figure it out yourself.
- **Find usages of a symbol** - Being able to see the context of a function being called can help your decision process when refactoring.

Mastering your tools will help you concentrate on the code and reduce context switching.

## Wrapping up

At this point you should have Go installed, an editor available and some basic tooling in place. Go has a very large ecosystem of third party products. We have identified a few useful components here, for a more complete list see https://awesome-go.com.
