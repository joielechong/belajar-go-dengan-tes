# Membangun sebuah aplikasi

Sekarang kamu telah diharapkan mencerna bagian _Dasar-dasar Go_ kamu telah mempunyai dasar yang kokoh tentang kebanyakan fitur bahasa Go dan bagaimana melakukan TDD.

Bagian selanjutnya akan mencakup pembangunan sebuah aplikasi.

Setiap bab akan mengulangi bab sebelumnya, memperluas fungsionalitas aplikasi seperti yang ditentukan oleh pemilik produk kita.

Konsep baru akan diperkenalkan untuk membantu memfasilitasi penulisan kode yang baik tetapi kebanyakan dari materi baru merupakan pembelajaran mengenai apa yang dapat dicapai dengan menggunakan pustaka standar Go.

Pada bagian akhir bab ini kamu akan memiliki pemahaman yang kuat tentang pembuatan aplikasi Go secara berkelanjutan dengan dukungan tes.

- [Server HTTP](http-server.md) - Kita akan membuat aplikasi yang akan mendengarkan permintaan HTTP dan menanggapinya.
- [JSON, routing and embedding](json.md) - Kita akan membuat endpoint yang mengembalikan JSON, menjelajahi tentang melakukan perutean dan mempelajarai mengenai penanaman.
- [IO (WIP)](io.md) - Kita akan menyimpan dan membaca data dari disk dan kita akan mempelajari penyortiran data.
